[settings]
throttle-output = 5
throttle-output-for = 10
throttle-input-for = 30
screenchange-reload = false

[colors]
background 	= #1e1e1f
foreground 	= #f8e2db
colour_1	= #e6c77d
colour_2	= #e6c77d
colour_3	= #CC6666

[bar/laptop]
width = 100%
height = 20
offset-x = 0
offset-y = 0
radius = 0.0
fixed-center = true

background = ${colors.background}
foreground =  ${colors.foreground}

line-size = 4

padding-left = 3
padding-right = 10

module-margin-left = 2
module-margin-right = 2

font-0 = Hack:fontformat=truetype:size=8;1
font-1 = Hack:fontformat=truetype:size=8;1
font-2 = FontAwesome:fontformat=truetype:size=8;1

modules-left = i3 xwindow
modules-center =
modules-right = pkg date battery

tray-position = right
tray-padding = 5
tray-offset-y = 0%
tray-offset-x = 0%
tray-maxsize = 18
tray-detached = true
tray-transparent = false
tray-color = #1D1F21

wm-restack = i3
override-redirect = false

[module/i3]
type = internal/i3
label-unfocused-background = ${colors.background}
label-unfocused-foreground = ${colors.foreground}
label-focused-font = 1
label-unfocused-font = 1

label-mode = %mode%
label-mode-padding = 2
label-mode-background = ${colors.colour_1}

label-focused = %index%
label-focused-foreground = ${colors.colour_2}
label-focused-background = ${colors.background}
label-focused-padding = 2

label-unfocused = %index%
label-unfocused-padding = 2

label-visible = %index%
label-visible-underline = ${colors.foreground}
label-visible-padding = 3

label-urgent = %index%
label-urgent-foreground = ${colors.foreground}
label-urgent-background = ${colors.background}
label-urgent-padding = 2

[module/xwindow]
type = internal/xwindow

[module/date]
type = internal/date
interval = 5

format = <label>
format-prefix-foreground = ${colors.foreground}

label = %date%, %time%

date = "%a %B %d"
date-alt = "%d-%m-%Y"

time-alt = "%I:%M %p"
time = "%H:%M"

[module/volume]
type = internal/volume

format-muted-background = ${colors.background}
format-volume-background = ${colors.background}
format-volume = <label-volume> <bar-volume>
label-muted = Muted

bar-volume-width = 15
bar-volume-foreground = ${colors.foreground}
bar-volume-gradient = false
bar-volume-indicator= ~
bar-volume-indicator-font = 1
bar-volume-fill = ─
bar-volume-fill-font = 1
bar-volume-empty = ─
bar-volume-empty-font = 1
bar-volume-empty-foreground = ${colors.colour_2}

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 65

format = <label>
format-warn = <label-warn>

label = %temperature%
label-warn = %temperature%
label-warn-foreground = ${colors.colour_3}

[module/powermenu]
type = custom/menu

format-spacing = 1

label-open = ⚡ Power
label-open-background = ${colors.background}
label-open-foreground = ${colors.foreground}
label-close = ⚡
label-close-background = ${colors.background}
label-close-foreground = ${colors.foreground}
label-separator = " "
label-separator-foreground = ${colors.foreground}

menu-0-0 = Reboot
menu-0-0-exec = shutdown -r now
menu-0-1 = Power Off
menu-0-1-exec = shutdown now

[global/wm]
margin-top = 5
margin-bottom = 5

[module/battery]
type = internal/battery
full-at = 100

battery = BAT0
adapter = AC
poll-interval = 5

time-format = %H:%M

format-charging = Charging: <label-charging>
format-discharging = Discharging: <label-discharging>
format-full = Battery: <label-full>

format-charging-foreground = ${colors.colour_1}
format-discharging-foreground = ${colors.colour_1}
format-full-foreground = ${colors.colour_1}

label-charging = %percentage%%
label-discharging = %percentage%%
label-full = Fully charged

label-charging-foreground = ${colors.foreground}
label-discharging-foreground = ${colors.foreground}
label-full-foreground = ${colors.foreground}

[module/wireless-network]
type = internal/network
interface = wlp2s0

interval = 5.0
accumulate-stats = true
format-connected = Wireless: <label-connected>
format-connected-foreground = ${colors.colour_1}
format-disconnected = <label-disconnected>

label-connected = %essid%  %local_ip%
label-connected-foreground = ${colors.foreground}

label-disconnected = (None)
label-disconnected-foreground = ${colors.foreground}

[module/vpn]
type = internal/network
interface = tun0

interval = 5.0
accumulate-stats = true
format-connected = VPN: <label-connected>
format-connected-foreground = ${colors.colour_1}
format-disconnected = <label-disconnected>

label-connected = Connected
label-connected-foreground = ${colors.foreground}

label-disconnected = (None)
label-disconnected-foreground = ${colors.foreground}

[module/pkg]
type = custom/script
interval = 1200
format = <label>
label = "%output:0:30%"
exec = .config/polybar/pkg.sh
exec-if = "ping -q -w 2 -c 1 176.34.135.167 > /dev/null"
